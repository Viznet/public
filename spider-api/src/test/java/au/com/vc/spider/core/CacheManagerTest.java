package au.com.vc.spider.core;

import au.com.vc.spider.model.TreeNode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;

@RunWith(SpringJUnit4ClassRunner.class)
public class CacheManagerTest {

    @InjectMocks
    private CacheManager cacheManager;

    @Before
    public void setup() {
        HashMap<String, TreeNode> cache = new HashMap<>();
        cache.put("http://www.aeo.com", new TreeNode("http://www.aeo.com", "AEO"));

        ReflectionTestUtils.setField(cacheManager, "cache", cache);
    }

    @Test
    public void shouldReturnFalseIfKeyNotFoundFromCache() {
        Assert.assertFalse(cacheManager.isAlreadyCrawled("www.avc.com"));
    }

    @Test
    public void shouldReturnTrueIfKeyFoundFromCache() {
        Assert.assertTrue(cacheManager.isAlreadyCrawled("http://www.aeo.com"));
    }

    @Test
    public void shouldReturnValueIfFoundFromCache() {
        TreeNode treeNode = cacheManager.getValue("http://www.aeo.com");
        Assert.assertNotNull(treeNode);
    }

}
