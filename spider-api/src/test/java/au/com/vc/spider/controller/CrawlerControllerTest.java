package au.com.vc.spider.controller;


import au.com.vc.spider.model.TreeNode;
import au.com.vc.spider.service.CrawlerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class CrawlerControllerTest {

    @InjectMocks
    private CrawlerController crawlerController;

    @Mock
    private CrawlerService crawlerService;

    private MockMvc mockMvc;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(crawlerController).build();
    }

    @Test
    public void testValidCrawlSeedUrlRequest() throws Exception {

        when(crawlerService.getSubPages(anyString())).thenReturn(new TreeNode("http://wwww.example.com", "Examples: Spider"));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/crawl?" + "seedUrl=http://wwww.example.com")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        verify(crawlerService, times(1)).getSubPages(anyString());
        verifyNoMoreInteractions(crawlerService);
    }

    @Test
    public void testInValidCrawlSeedUrlRequest() throws Exception {

        when(crawlerService.getSubPages(anyString())).thenReturn(new TreeNode("http://wwww.example.com", "Examples: Spider"));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/crawl")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andReturn();

    }
}
