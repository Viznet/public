package au.com.vc.spider.core;

import org.junit.Assert;
import org.junit.Test;

public class SpiderUtilTest {

    @Test
    public void testIsValidUrl(){

        Assert.assertFalse(SpiderUtil.isValidUrl("http://www.abc.com.au/style/myStyle.css"));
        Assert.assertFalse(SpiderUtil.isValidUrl("http://www.abc.com.au/pdf/myPdf.pdf"));
        Assert.assertFalse(SpiderUtil.isValidUrl("http://www.abc.com.au/js/abc.js"));
        Assert.assertFalse(SpiderUtil.isValidUrl("http://www.abc.com.au/zip-files/amyZip.zip"));
        Assert.assertFalse(SpiderUtil.isValidUrl("http://www.abc.com.au/image/myImage.png"));
        Assert.assertFalse(SpiderUtil.isValidUrl("mailto:return WebForm_OnSubmit()"));

    }

    @Test
    public void isValidDomainLink(){
        Assert.assertTrue(SpiderUtil.isValidDomainLink("http://www.abc.com/support/support.html", "http://www.abc.com"));
        Assert.assertTrue(SpiderUtil.isValidDomainLink("https://www.abc.com/support/support.html", "http://www.abc.com"));
        Assert.assertFalse(SpiderUtil.isValidDomainLink("https://www.xyz.com/support/support.html", "http://www.abc.com"));
    }
}
