package au.com.vc.spider.core;


import au.com.vc.spider.model.TreeNode;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;


@RunWith(PowerMockRunner.class)
@PrepareForTest({Jsoup.class})
public class CrawlerTest {

    @InjectMocks
    private Crawler crawler;

    @Before
    public void setup() throws Exception {
        StringBuffer html = new StringBuffer();
        //a sample html page containing links
        html.append("<!DOCTYPE html>");
        html.append("<html lang=\"en\">");
        html.append("<head>");
        html.append("<meta charset=\"UTF-8\" />");
        html.append("<title> Sydney Forex :: (Send, Transfer, Remitt) Money to anywhere worldwide from Australia</title>");
        html.append("</head>");
        html.append("<body>");
        html.append("body content");
        html.append(" <a href=https://wwww.sydneyforex.com.au/default.aspx\">HOME</a>  \n" +
                " <a target=\"_blank\" href=\"Documents/ServiceandCharges.pdf\" >SERVICES AND CHARGES</a>  \n" +
                " <a href=https://wwww.sydneyforex.com.au/news.aspx\">NEWS/EVENTS</a>  \n" +
                " <a href=mailto:return WebForm_OnSubmit();>BANK DETAILS</a>  \n" +
                " <a href=javascript:return WebForm_OnSubmit()>CONTACT US</a>\n");
        html.append("body content again");
        html.append("</body>");
        html.append("</html>");

        Document doc = Jsoup.parse(html.toString());

        PowerMockito.mockStatic(Jsoup.class);

        Connection conn = mock(HttpConnection.class);
        when(Jsoup.connect(anyString())).thenReturn(conn);
        when(conn.get()).thenReturn(doc);
    }

    @Test
    public void shouldReturnSubLinksAsTreeNode() {
        TreeNode treeNode = crawler.crawlJob("https://wwww.sydneyforex.com.au/");
        Assert.assertNotNull(treeNode);
        Assert.assertEquals(1, treeNode.getChildren().size());

    }
}
