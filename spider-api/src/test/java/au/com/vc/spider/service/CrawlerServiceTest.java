package au.com.vc.spider.service;

import au.com.vc.spider.core.CacheManager;
import au.com.vc.spider.core.Crawler;
import au.com.vc.spider.model.TreeNode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class CrawlerServiceTest {

    @InjectMocks
    private CrawlerServiceImpl crawlerService;

    @Mock
    private Crawler crawler;

    @Mock
    private CacheManager cacheManager;

    private TreeNode treeNode;

    @Before
    public void setup() {
        treeNode = new TreeNode("example.com", "Example");
        treeNode.addChild(new TreeNode("child.com", "Child Link"));
    }

    @Test
    public void shouldReturnSubLinksAsTreeNode() {
        when(crawler.crawlJob(anyString())).thenReturn(treeNode);
        TreeNode treeNode = crawlerService.getSubPages("http://www.sydneyforex.com.au");
        Assert.assertNotNull(treeNode);
        Assert.assertEquals(1, treeNode.getChildren().size());
    }

    @Test
    public void shouldReturnTreeNodeIfAlreadyCrawled() {
        when(cacheManager.isAlreadyCrawled("http://www.sydneyforex.com.au")).thenReturn(true);
        when(cacheManager.getValue("http://www.sydneyforex.com.au")).thenReturn(treeNode);
        TreeNode treeNode = crawlerService.getSubPages("http://www.sydneyforex.com.au");
        Assert.assertNotNull(treeNode);
        Assert.assertEquals(1, treeNode.getChildren().size());
    }

}

