package au.com.vc.spider.core;


import au.com.vc.spider.model.TreeNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashSet;


/**
 * Class will provide the implementation for crawler algorithm using Jsoup.
 */
@Component
public class Crawler {

    private static Logger logger = LoggerFactory.getLogger(Crawler.class);

    private HashSet<String> crawledUrls = null;

    private String seedUrl;

    /**
     * Takes seed url and crawl all sub links and prepare tree structure for sub links.
     * @param seedUrl
     * @return instance of TreeNode
     */
    public TreeNode crawlJob(final String seedUrl){
        this.seedUrl = seedUrl;
        this.crawledUrls = new HashSet<>();
        return crawl(seedUrl);
    }

    private TreeNode crawl(final String url) {

        if (!StringUtils.isEmpty(url) && !this.crawledUrls.contains(url) && SpiderUtil.shouldVisit(url, this.seedUrl)) {

            logger.info("Link: [{}]", url);
            try {

                final Document document = Jsoup.connect(url).get();
                final Elements linksOnPage = document.select("a[href]");
                final TreeNode node = new TreeNode(url, document.title());
                this.crawledUrls.add(url);

                for (final Element page : linksOnPage) {

                    node.addChild(crawl(page.attr("abs:href")));
                }

                return node;
            } catch (IllegalArgumentException ie) {
                logger.error("Could not process Malformed URL [{}] : {}.", url, ie.getMessage());
                throw new ServiceException(ie.getMessage(), ie);
            } catch (IOException e) {
                logger.error("Could not process URL [{}] : {}.", url, e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }
        }
        return null;
    }
}