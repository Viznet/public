package au.com.vc.spider.model;

import java.io.Serializable;

public class ApiError implements Serializable {

    private static final long serialVersionUID = -9029808970528749990L;

    private String error;
    private String description;

    public ApiError(String error, String description) {
        this.description = description;
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}