package au.com.vc.spider.controller;


import au.com.vc.spider.model.TreeNode;
import au.com.vc.spider.service.CrawlerService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class CrawlerController {

    private static final Logger logger = LoggerFactory.getLogger(CrawlerController.class);

    @Autowired
    private CrawlerService crawlerService;

    @ApiOperation(value = "List all sub links for provided seed URL as tree structure.", response = TreeNode.class)
    @RequestMapping(value = "/crawl",
            produces = {MediaType.APPLICATION_JSON_UTF8_VALUE},
            method = RequestMethod.GET)
    public ResponseEntity<TreeNode> crawl(@RequestParam(value = "seedUrl") String seedUrl) {
        logger.info("Crawl request received for seed url {}", seedUrl);
        return ResponseEntity.ok().body(crawlerService.getSubPages(seedUrl));
    }
}