package au.com.vc.spider.core;

import au.com.vc.spider.model.TreeNode;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;

/**
 * Simple cache solution to keep crawled result.
 */
@Component
public class CacheManager {

    //store the result of seed url including all children
    private HashMap<String, TreeNode> cache = new HashMap<>();

    public TreeNode getValue(final String key) {
        return this.cache.get(key);
    }

    public boolean isAlreadyCrawled(final String key) {
        return this.cache.containsKey(key);
    }

    public void add(final String key, final TreeNode treeNode) {
        if (!StringUtils.isEmpty(key) || null != treeNode)
            this.cache.put(key, treeNode);
    }
}