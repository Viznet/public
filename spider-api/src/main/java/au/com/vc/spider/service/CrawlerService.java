package au.com.vc.spider.service;

import au.com.vc.spider.model.TreeNode;

public interface CrawlerService {
    /**
     * Takes seed url and recursively find out all sub links for passed
     * url domain.
     * @param url
     * @return TreeNode containing sub links
     */
    TreeNode getSubPages(String url);
}

