package au.com.vc.spider.service;

import au.com.vc.spider.core.CacheManager;
import au.com.vc.spider.core.Crawler;
import au.com.vc.spider.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrawlerServiceImpl implements CrawlerService {

    private static Logger logger = LoggerFactory.getLogger(CrawlerServiceImpl.class);

    @Autowired
    private CacheManager simpleCacheManager;

    @Autowired
    private Crawler crawler;

    @Override
    public TreeNode getSubPages(final String url) {

        if (simpleCacheManager.isAlreadyCrawled(url)) {
            logger.info("Already crawled provided seed url {}, returning from cache.", url);
            //if found return from temp cache
            return simpleCacheManager.getValue(url);
        } else {
            final TreeNode treeNode = crawler.crawlJob(url);
            simpleCacheManager.add(url, treeNode);
            return treeNode;
        }
    }
}
