package au.com.vc.spider.controller;

import au.com.vc.spider.model.ApiError;
import au.com.vc.spider.core.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice(annotations = RestController.class)
public class CrawlerControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(CrawlerControllerAdvice.class);

    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity handleServiceException(ServiceException e) {
        logger.error("Service exception occurred. Exception: ", e);
        return ResponseEntity.badRequest().body(new ApiError("Bad Request parameter.", "Could not proceed with seed url."));
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseEntity handleException(Exception e) {
        logger.error("An error occurred: ", e);
        return ResponseEntity.badRequest().body(new ApiError("Internal Server Error.", "Could not proceed with seed url."));
    }
}
