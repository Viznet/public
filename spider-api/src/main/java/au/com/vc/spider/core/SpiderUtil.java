package au.com.vc.spider.core;

import java.util.regex.Pattern;

/**
 * Utility class
 */
public class SpiderUtil {

    private final static Pattern FILTERS = Pattern.compile(
            ".*(\\.(css|js|bmp|gif|jpe?g"
                    + "|png|tiff?|mid|mp2|mp3|mp4"
                    + "|wav|avi|mov|mpeg|ram|m4v|pdf"
                    + "|rm|smil|wmv|swf|wma|zip|rar|gz))$"
    );


    /**
     * Utility method to check whether current url is candidate for crawling.
     *
     * @param url
     * @param seedUrl
     * @return
     */
    public static boolean shouldVisit(final String url, final String seedUrl) {
        return isValidUrl(url) && isValidDomainLink(url, seedUrl);
    }

    /**
     * Utility method to filter current url for non page link.
     *
     * @param url
     * @return boolean
     */
    public static boolean isValidUrl(final String url) {
        if(url.startsWith("javascript:"))  { return false; }
        if(url.contains("mailto:"))        { return false; }
        return !FILTERS.matcher(url).matches();
    }


    /**
     * Utility method to check current url is belong to seed url or not.
     *
     * @param url
     * @param seedUrl
     * @return boolean
     */
    public static boolean isValidDomainLink(final String url, final String seedUrl) {
        Pattern pattern = Pattern.compile("^(http|https)://");
        return pattern.matcher(url).replaceFirst("").startsWith(pattern.matcher(seedUrl).replaceFirst(""));
    }
}