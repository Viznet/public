package au.com.vc.spider.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TreeNode implements Serializable {

    public static final long serialVersionUID = 8806961455825508901L;

    private String url;
    private String title;
    private List<TreeNode> children = new ArrayList<>();

    public TreeNode(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public void addChild(TreeNode child) {
        if (null != child) {
            this.children.add(child);
        }
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}