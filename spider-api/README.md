#Spider API

 Spider API is a Spring Boot micro-service (web crawling service) to crawl provided seed url and list links for sub pages (only belong to same domain). The artifact is an executable jar file. Java 8 is the required JDK. IntelliJ IDEA is *strongly* recommended.
It has very basic level of cache to hold crawl result, if seedUrl is request again it will serve with existing crawling result from cache.

## Used Dependencies

*Java 8

*Apache Maven:3.5.2

*Spring-boot:1.5.9.RELEASE

*jsoup:1.10.3

*springfox-swagger-ui:2.7.0

*springfox-swagger2:2.7.0


## Building the Spider API micro-service

To build the application run the following command from the root of the project directory:

```text
mvn clean verify
```

## Running from the command line

Download zip file including source and artifact spider-api-0.0.1-SNAPSHOT.jar and run following command. To successfully run Java 8 is required.

```text
java -jar spider-api-0.0.1-SNAPSHOT.jar
```
##Spider API - Web Crawler  
To access crawl REST service (where 'seedUrl' is required with GET as request method) use any REST Client tool to test service or hit following Swagger link.
 
 
 http://localhost:8080/spider/swagger-ui.html
 
 http://localhost:8080/spider/swagger-ui.html#!/crawler45controller/crawlUsingGET
 
 Or directly hit following URL using any browser after running service.
 
 http://localhost:8080/spider/api/v1/crawl?seedUrl=https://www.sydneyforex.com.au

 ```text
 @Example for seed url : seedUrl=https://www.sydneyforex.com.au
 ```
 
 Response will be Json containing a tree structure for all sub links.
 
 ```json
 {
   "url":"http://www.sydneyforex.com.au",
   "title":"Sydney Forex :: (Send, Transfer, Remitt) Money to anywhere worldwide from Australia",
   "children":[
     {
       "url":"https://www.sydneyforex.com.au/default.aspx",
       "title":"Sydney Forex :: (Send, Transfer, Remitt) Money to anywhere worldwide from Australia",
       "children":[
         {
           "url":"https://www.sydneyforex.com.au/about.aspx",
           "title":"SydneyForex:: About Us",
           "children":[
           ]
         },
         {
           "url":"https://www.sydneyforex.com.au/RegisterLogin.aspx",
           "title":"SydneyForex:: Member Registration",
           "children":[
           ]
         }]
     },
     {
       "url":"https://www.sydneyforex.com.au/#",
       "title":"Sydney Forex :: (Send, Transfer, Remitt) Money to anywhere worldwide from Australia",
       "children":[
       ]
     }]
 }
 ```

