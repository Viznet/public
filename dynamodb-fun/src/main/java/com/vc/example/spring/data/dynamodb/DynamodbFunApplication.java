package com.vc.example.spring.data.dynamodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamodbFunApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamodbFunApplication.class, args);
	}
}
