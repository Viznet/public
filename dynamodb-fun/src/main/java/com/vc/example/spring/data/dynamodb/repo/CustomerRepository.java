package com.vc.example.spring.data.dynamodb.repo;

import com.vc.example.spring.data.dynamodb.model.Customer;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface CustomerRepository extends CrudRepository<Customer, String> {


    Customer findById(String id);

    List<Customer> findByLastName(String lastName);

}
