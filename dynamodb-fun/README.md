

Getting started with DynamoDB Local is very simple. Go to Amazon’s website and download the DynamoDB Local executable and install it along with the latest version of the JDK. Now you just run the command below to bring up the database.

1
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb -port 8888
This will start open DynamoDB on your machine at port 8888. Now to access the DynamoDB console, just go to http://localhost:8888/shell

###Create DynamoDB table
– Run DynamoDB:
```
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
```

```
aws dynamodb create-table 
    --table-name Customer 
    --attribute-definitions AttributeName=Id,AttributeType=S 
    --key-schema AttributeName=Id,KeyType=HASH 
    --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 
    --endpoint-url http://localhost:8000
```
 
 aws dynamodb create-table --cli-input-json file://create-table-movies.json --endpoint-url http://localhost:8000


 aws dynamodb create-table --table-name Customer --attribute-definitions AttributeName=Id,AttributeType=S --key-schema AttributeName=Id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 --endpoint-url http://localhost:8000